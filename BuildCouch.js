import fs from 'node:fs';
import path from 'node:path';
import push from 'couchdb-push';
import * as global from './global.js';
import * as utils from './utils.js';

export{
  BuildDb
}

async function BuildDb(){
	let builds = [];
	let rootpath = path.resolve('.','schema');
	let folders = fs.readdirSync(rootpath, { withFileTypes: true });
	for(let folder of folders){
		if(!folder.isDirectory()) continue;
		folder = path.resolve(rootpath,folder.name);
		let pushed = new Promise(function (resolve){
			let url = global.settings.storage.ibmmnb.url;
			push(url, folder, function(err, resp) {
				resolve(resp);
			});
		});
		builds.push(pushed);
		await pushed;
		await utils.Pauser();
	}
	await Promise.all(builds);
	return builds;
}

async function DeleteMistakes(){
	/*
	let recs = await global.sec.query('mnb/fordelete',{reduce:false,limit:5000,include_docs:true,});
	if(recs.rows.length === 0)return;
	let now = Date.now();
	recs = recs.rows.map(d=>{
		return {
			_id: d.doc._id,
			_rev: d.doc._rev,
			_deleted: true,
			"@": now,
		}
	});
	global.sec.bulkDocs(recs)
		.then(()=>{
			console.log('Successfully Deleted deletes.');
		})
		.catch((e)=>{
			console.error(e);
		});
	*/
}

BuildDb();
DeleteMistakes();
