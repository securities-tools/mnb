function(doc){
	if('score' in doc) return;

	var futureDays = doc.bug.resp.predFin;
	var finalDate = new Date(doc.bug.req.date);
	finalDate.setDate(finalDate.getDate() + futureDays);
	finalDate = finalDate.getTime();

	emit(finalDate, doc._id);
}
