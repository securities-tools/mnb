function (doc) {
	if (!('score' in doc)) return;
	if(doc.score === null) return;
	var gen = doc.genome.generation || 0;
	gen = Math.floor(gen).toString(10);
	gen = ('000000000000000' + gen)
		.split('')
		.reverse()
		.slice(0,6)
		//.reverse()
		;
	var key = [];
	while(gen.length > 0){
		key.push(parseInt(gen.pop(),10));
	}
	emit(key, doc.score);
}
