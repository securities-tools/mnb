function (doc) {
	if(doc._id.split('.').shift() !== 'form') return;
	var keep = [
		//1000069,
		//1000177,
		6769
	];
	if(keep.indexOf(doc.cik) < 0) return;

	var values = doc.submissions;
	if (!values) return;
	values = values[0];
	if (!values) return;
	values = values.values;
	if (!values) return;
	var keepTypes = [
		'boolean',
		'number',
		'string'
	];
	var skipFields = [
		'asat',
		'startdate',
		'endDate',
		'currentfiscalyearenddate',
		'documentfiscalyearfocus',
		'documentq4focus',
		'entityregistrantname'
	];
	var keyFields = [
		'EntityCentralIndexKey',
		'DocumentPeriodEndDate',
	];

	var rgxContext = new RegExp('.*context.*');
	var rgxCouchFld = /^_/;
	function isAllowed(f,val){
		f = f.toLowerCase();
		if (rgxCouchFld.test(f)) return false;
		if (rgxContext.test(f)) return false;
		if (keepTypes.indexOf(typeof val) < 0) return false;
		if (skipFields.indexOf(f) >= 0) return false;
		return true;
	}

	/**
	 * Hashes for converting strings into numbers (deterministically)
	 */
	function bernstein(key){
		var hash = 0;
		for (var i=0; i<key.length; i++){
			hash = 33*hash + key.charCodeAt(i);
		}
		return hash%131071;
	}
	function unihash(f){
		//return f;
		return bernstein(f);
	}
  /**
   *
   */
  function HeapPermute(len, list, agg){
    agg = agg || [];
    if (len <= 0){
        agg.push([list.slice()]);
        return agg;
    }
    HeapPermute(len-1, list, agg);
    for(var i= 0; i < len; i++){
      // Swap choice dependent on parity of k (even or odd)
      var pos = (len%2) ? 0 : i;
      var tmp = list[pos];
      list[pos] = list[len];
      list[len] = tmp;
      HeapPermute(len-1, list, agg);
    }
    return agg;
  }

	/**
	 * All values must be numeric... but some aren't
	 * to resolve this, we are going to co-erce values into
	 * their numeric equivalent, but that takes some smarts too
	 * Try:
	 *  1. Number
	 *  2. Date
	 *  3. Boolean
	 *  4. String
	 */
	function ToNumeric(val){
		if(typeof val === 'number'){
			return {
				value: val,
				datatype: 'number'
			}
		}

		var converted = null;
		converted = parseFloat(val);
		if( !isNaN(converted) && isFinite(converted) || val.replace(/^-/,'').replace(/[^-]/g,'').length <= 0 ){
			return {
				value: converted,
				datatype: 'number'
			};
		}

		converted = Date.parse(val);
		if(!isNaN(converted)){
			return {
				value: converted,
				datatype: 'date'
			};
		}

		if(val === 'true'){
			return { value: 1, datatype: 'boolean' };
		}
		else if(val === 'false'){
			return { value: 0, datatype: 'boolean' };
		}

		return {
			value: unihash(val),
			datatype: 'string'
		};
	}

	var keys = {};
	var vals = {};

	for(var f in values){
		if( !isAllowed(f,values[f]) ) continue;
		var val = ToNumeric(values[f]);
		var group = vals;
		if(val.datatype !== 'number' || keyFields.indexOf(f)>=0){
			group = keys;
		}
		group[f] = val.value;
	}

	// The intent is to get the keys in a deterministic state for the
	// default state. We want them to always appear in the same order
	// even if new keys are added.
	//
	// keys = Object.entries(keys)
	var key = [];
	for(var k in keys){
		key.push([k,keys[k]]);
	}
	keys = key;
	keys.sort(function(a,b){return a[1]-b[1];});
	key = [];
	for(var k in keys){
		k = keys[k][1];
		//k = keys[k][0];
		key.push(k);
	}

	for(var k in vals){
		key.push(unihash(k));
		emit(key,vals[k]);
		//var combos = HeapPermute(key.length-1,key);
		//for(var c in combos){
		//  emit(combos[c], vals[k]);
		//}
		key.pop();
	}

}
