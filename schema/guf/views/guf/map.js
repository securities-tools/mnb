function (doc) {
	if(doc.ova > 0){
    var id = doc._id.length-4;
    id = doc._id.substr(id,4);
    id = parseInt(id,16);
		emit(
			[
				doc.genome.karma||0,
				doc.score,
				2000000000-doc.genome.generation,
				id
			],
			doc.ova
		);
	}
}
