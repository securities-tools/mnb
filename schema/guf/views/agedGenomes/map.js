function (doc) {
	var key = [
		doc.asat || 0,
		doc.score || Math.pow(2^31)
	];
	emit(key, null);
}
