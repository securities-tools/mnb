import fs from 'node:fs';
import os from 'node:os';

import * as global from  '../../global.js';
import * as utils from '../../utils.js';
import { BuildDb } from '../../BuildCouch.js';

export{
	readState,
	saveBug,
	getBug,
	destroy,
	db,
	maxItems,
	WaitPaused,
	isPaused,
};


const db = global.dbs;

let extinction = 0;
let shouldCompact = false; // should be true, but causes app to crash
let maxItems = 100;
let isPausedFlag = false;

function isPaused(){
	return !!isPausedFlag;
}
async function WaitPaused(){
	while(isPaused()){
		await utils.wait(10000);
	}
}

function destroy(){
	db.guf.destroy();
}

async function readState(){
	try{
		await WaitPaused();
		let data = await db.guf.allDocs({
			limit:1,
			startkey: '_local/settings',
			endkey: '_local/settings',
			include_docs:true
		});
		data = data.rows.pop() || {};
		return data;
	}
	catch(e){
		console.debug(e);
		debugger;
	}
}

async function getBug(gufPos){
	await WaitPaused();
	let gen = utils.Pauser(()=>{return db.guf.query('guf',{
		skip: gufPos,
		limit: 1,
		descending: true,
		attachments: true,
		reduce: false,
		include_docs:true,
	})});
	return gen;
}

async function saveBug(bug){
	let resp = {
		done: bug.actuators.done,
		predict: bug.actuators.predict,
		predChg: bug.actuators.predChg,
		predStart: bug.actuators.predStart,
		predFin: bug.actuators.predFin,
	};
	let blob = null;
	blob = Buffer.from(bug.genome.buffer);

	let rec = {
		type: 'animat',
		genome: {
			parents: Array.from(bug.genome.meta.parents.values()),
			karma: bug.genome.meta.karma,
			generation: bug.genome.meta.generation
		},
		bug: {
			id: bug.id,
			req: bug.sensorConstants,
			resp: resp,
			age: bug.age,
		},
		asat: Date.now(),
		_attachments:{
			genome:{
				content_type: 'application/octet',
				data: blob
			}
		}
	};

	await WaitPaused();
	for(let tries = 3; tries>0; tries--){
		try{
			await db.guf.post(rec);
			await cleanGuf();
			tries = -1;
		}
		catch(e){
			console.debug(e);
		}
	}
}


/**************
 * CLEAN THE GUFF
 *
 * At some point, we need to get rid of items
 * that are not going to do anything. This is
 * purely to conserve space.
 */
async function cleanGuf(){
	/**************
	 * The extinction counter allows us to do
	 * clean up as a bulk operation. Rather than
	 * cleaning up on each pass, we clean up
	 * infrequently to allow it to save effort.
	 *
	 * We clean up 200 items, every 100 items.
	 * This way we clean up more mess than we
	 * make.
	 */
	if(--extinction>0){
		return;
	}
	extinction = 10;

	/**************
	 * STERILIZE THE BOTTOM
	 *
	 * The first step is to sterilize the worst
	 * of our genomes. Sorted by score, take the
	 * worst and sterilize them.
	 **************/
	let recs = await utils.Pauser(()=>{ return db.guf.query('guf/guf',{
		skip:maxItems,
		limit:200,
		reduce:false,
		include_docs:true,
		descending:true
	})});
	let rows = [];
	for(let r of recs.rows){
		r.doc.ova = 0;
		rows.push(r.doc);
	}
	if (rows.length > 0) {
		await utils.Pauser(()=>{ return db.guf.bulkDocs(rows); })
	}

	/**************
	 * REMOVE USELESS GENOMES
	 *
	 * Genomes without ova are pointless. Delete the
	 * genomes if ova have gone to zero
	 **************/
	recs = await utils.Pauser(()=>{ return db.guf.query('guf/uselessGenomes',{
		limit:200,
		reduce:false,
		include_docs:true
	})});
	rows = [];
	for(let r of recs.rows){
		r = createDeleteDocument(r)
		rows.push(r);
	}
	if (rows.length > 0){
		await utils.Pauser(()=>{ return db.guf.bulkDocs(rows); });
	}


	/**********
	 * CLEAN OLD GENOMES
	 *
	 * In the event a lot of items have built up,
	 * just remove a bunch. We define this as 10x
	 * the number of buffered items, and remove a
	 * significant number of items.
	 */
	/*
	recs = await utils.Pauser(()=>{ return db.guf.query('guf/guf',{
		skip:maxItems,
		limit:100,
		reduce:false,
		descending: false,
		include_docs:true
	})});
	rows = [];
	for(let r of recs.rows){
		r = createDeleteDocument(r);
		rows.push(r);
	}
	await utils.Pauser(()=>{ return db.guf.bulkDocs(rows); });
	*/

	/**********************
	 * As we have deleted stuff, we should
	 * compact the database
	 *
	 * Some providers throw an error when issuing
	 * a `compact` because they don't support the
	 * operation. In this case, catch the error
	 * and turn off future attempts.
	 */
	let info = await db.guf.info();
	if(info.doc_count/2 < info.doc_del_count){
		console.log('Pack..: ');
		try{
			// HACK: the compact breaks shit, and must therefore never run
			shouldCompact = false;
			isPausedFlag = true;
			if(shouldCompact){
				// This line of code breaks shit... I don't understand why
				await db.guf.compact();
			}
			else{
				let tmpFolder = os.tmpdir();
				tmpFolder = fs.mkdtempSync(`${tmpFolder}/guf-`);
				let buffSize = 10;
				// alternate compaction routine
				let ismore = true;
				for(let pos = 0; ismore; pos+=buffSize){
					recs = await utils.Pauser(()=>{ return db.guf.query('guf/guf',{
						skip: pos,
						limit: buffSize,
						attachments: true,
						include_docs: true,
						reduce:false,
					})});
					for(let rec of recs.rows){
						fs.writeFileSync(`${tmpFolder}/${rec.id}`,JSON.stringify(rec.doc));
					}
					ismore = !!recs.rows.length;
				}
				await db.guf.destroy();
				await db.guf.connect();

				await BuildDb();

				let files = fs.readdirSync(tmpFolder);
				let waitin = 0;
				let buffer = [];
				for(let file of files){
					let content = fs.readFileSync(`${tmpFolder}/${file}`);
					content = content.toString();
					content = JSON.parse(content);
					delete content._rev;
					buffer.push(
						db.guf.put(content).catch((err)=>{
							console.error(err);
						})
					);
					if(--waitin <= 0){
						await Promise.all(buffer);
						buffer = [];
						await utils.Pauser(()=>{
							waitin=9;
						})
					}
				}
				await Promise.all(buffer);
			}
		}
		catch(e){
			debugger;
			shouldCompact = false;
			console.error(e);
		}
		finally{
			isPausedFlag = false;
		}
	}
}


function createDeleteDocument(r){
	r = r.doc;
	//for(let f in r){
	//	if(f.startsWith('_')) continue;
	//	delete r[f];
	//}
	r._deleted = true;
	r.asat = Date.now();
	return r;
}
