export {
	stats as default
}

const stats = [
	Count,
	Sum,
	Mean,
	Median,
	mode,
	stddev,
	absdev,
	min,
	max,
];


function Count(g) {
	return g.inputs.length;
}


function Sum(g, state) {
	let sum = 0;
	for (let i of g.inputs) {
		let s = state[i]
		sum += s;
	}
	return sum;
}

function Mean(g,state){
	let count = Count(g,state);
	let avg = 0;
	for (let i of g.inputs) {
		let s = state[i]
		s /= count;
		avg += s;
	}
	return avg;
}

function Median(g,state){
	// copy all the values from memory into a new array
	let values = g.inputs.slice();
	for(let v in values){
		let index = values[v];
		values[v] = state[index];
	}
	// sort it
	values = values.sort((a,b)=>{return a-b;});
	// find the middle and grab the middle value from the top and the bottom.
	let mid = Math.ceil((values.length)/2)-1;
	let low = values[mid];
	mid = values.length-1-mid;
	let high = values[mid];
	// find their average
	return (high+low)/2;
}

function mode(g,state){
	let values = new Map();
	for(let pos of g.inputs){
		let value = state[pos];
		if(!values.has(value)){
			values.set(value,0);
		}
		values.set(value,values.get(value)+1);
	}
	values = Array.from(values.entries());
	values.sort((a,b)=>{
		return a[1]-b[1];
	});
	let mode = [values.pop()];
	for(let val=values.pop(); val; val= values.pop()){
		if(mode[0][1] !== val[1]){
			break;
		}
		mode.push(val);
	}
	mode = mode.map(m=>{return m[0];});
	mode = mode.reduce((a,d)=>{
		a += d/mode.length;
		return a;
	},0);
	return mode;
}

function stddev(g,state){
	let mean = Mean(g,state);
	let dev = 0;
	for (let s of g.inputs) {
		let diff = state[s];
		diff -= mean;
		diff = Math.pow(diff,2);
		diff /= g.inputs.length;
		dev += diff;
	}
	dev = Math.pow(dev,0.5);
	return dev;
}

function absdev(g,state){
	let mean = Mean(g,state);
	let dev = 0;
	for (let s of g.inputs) {
		let diff = state[s];
		diff -= mean;
		diff = Math.abs(diff);
		diff /= g.inputs.length;
		dev += diff;
	}
	return dev;
}

function min(g,state){
	let m = Number.MAX_VALUE;
	for(let s of g.inputs){
		let v = state[s];
		m = Math.min(m,v);
	}
	return m;
}

function max(g,state){
	let m = Number.MIN_VALUE;
	for(let s of g.inputs){
		let v = state[s];
		m = Math.max(m,v);
	}
	return m;
}
