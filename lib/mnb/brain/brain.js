import { GateTypes } from './gates.js';

export{
	Brain as default,
	Brain,
	BrainIO
};

const min_brainSize = 1024 / Uint32Array.BYTES_PER_ELEMENT;
const max_brainSize = min_brainSize**3;
const min_number_gates = min_brainSize;
const max_number_gates = min_number_gates*2;
const min_number_inputs = 1;
const max_number_inputs = min_brainSize;
const min_number_outputs = 1;
const max_number_outputs = 4;

const ElementMax = 2 ** (Uint32Array.BYTES_PER_ELEMENT*8-1);

class BrainIO extends Uint32Array{
	constructor(brain, offset, elements){
		offset *= 4;
		super(brain.state.buffer,offset,elements);
	}

	reset(value=ElementMax){
		this.fill(value);
	}

	asInt(pos)   { return BrainIO.toInt(this[pos]); }
	asFloat(pos) { return BrainIO.toFloat(this[pos]); }
	asBool(pos)  { return BrainIO.toBoolean(this[pos]); }
	asRange(pos,size) { return BrainIO.toRange(this[pos],size); }

	static toInt(value){
		let v = value;
		v -= ElementMax;
		return v;
	}

	static toFloat(value){
		let v = value;
		v = BrainIO.toInt(v);
		v = v / ElementMax;
		return v;
	}

	static toRange(value,size=100){
		let v = BrainIO.toFloat(value);
		v += 1;
		v /= 2;
		v *= size;
		v = Math.floor(v);
		return v;
	}

	static toBoolean(value){
		let v = value;
		v /= ElementMax;
		v *= 2;
		v -= 1;
		v = !!v;
		return v;
	}

}

class Brain{
	constructor(genome, gates=GateTypes.bitGateTypes){
		this.state = CreateMemory(genome);
		this.gates = CreateGates(genome, this.state, gates);
	}

	think(sensorData=[]) {
		let mind = this.state;
		for (let k = 0; k < sensorData.length; k++) {
			mind[k] = sensorData[k];
		}

		for (let gate of this.gates) {
			let result = gate.handler(gate, mind);
			result = Math.floor(result);
			for (let o of gate.outputs) {
				mind[o] = result;
			}
		}

		return mind;
	}

	get hash(){
		let rgbSize = Math.pow(2,3*8);
		let rgb = this.state.reduce((a,d)=>{
				a *= 2;
				a += Math.floor(a/rgbSize);
				a %= rgbSize;
				a ^= d;
				return a;
			},0);
		return rgb;
	}
}

class Gate {
	constructor(genome,memory,gateTypes){
		this.type = genome.nextRatio;
		this.type *= gateTypes.length;
		this.type = Math.floor(this.type);

		this.inputs = wireNeurons(genome, memory.length, min_number_inputs, max_number_inputs);
		this.outputs = wireNeurons(genome, memory.length, min_number_outputs, max_number_outputs);

		this.handler = gateTypes[this.type];
	}
}


function CreateGates(genome,memory,gateTypes) {
	let qty = genome.nextRatio;
	qty *= max_number_gates - min_number_gates;
	qty += min_number_gates;
	qty = Math.floor(qty);

	let gates = [];
	for(let i = 0; i<qty; i++){
		let gate = new Gate(genome,memory,gateTypes);
		gates.push(gate);
	}
	return gates;
}


function CreateMemory(genome) {
	let qty = genome.nextRatio;
	qty *= max_brainSize - min_brainSize;
	qty += min_brainSize;
	qty = Math.floor(qty);

	let buf = 0;
	let states = new Uint32Array(qty);
	for (let k = 0; k < qty; k++) {
		buf = genome.next;
		states[k] = buf;
	}

	return states;
}


function wireNeurons(genome, mindSize, minWirings, maxWirings) {
	let qty = genome.nextRatio;
	qty *= maxWirings - minWirings;
	qty += minWirings;
	qty = Math.floor(qty);

	let wirings = [];
	for(let i = 0; i<qty; i++){
		let w = genome.nextRatio;
		w *= mindSize;
		w = Math.floor(w);
		wirings.push(w);
	}
	return wirings;
}
