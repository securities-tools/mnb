export{
	bitGateTypes,
	GateTypes as default,
	GateTypes
}

class gateTypes extends Array{
	constructor(){
		super();
	}
}


const bitGateTypes = [
	xor,
	or,
	and,
	nor,
	nand
];
//const GateTypes = new gateTypes();
const GateTypes = bitGateTypes;

function xor(g1, state) {
	let sum = 0;
	for (let i of g1.inputs) {
		let s = state[i];
		sum ^= s;
	}
	return sum;
}


function or(g1, state) {
	let sum = 0;
	for (let i of g1.inputs) {
		let s = state[i];
		sum |= s;
	}
	return sum;
}


function and(g1, state) {
	let sum = MaxMask;
	for (let i of g1.inputs) {
		let s = state[i];
		sum &= s;
	}
	return sum;
}


function nor(g1, state) {
	let sum = 0;
	for (let i of g1.inputs) {
		let s = state[i];
		s = ~s;
		s = Math.abs(s);
		sum |= s;
	}
	return sum;
}


const MaxMask = 2**32-1;
function nand(g1, state) {
	let sum = MaxMask;
	for (let i of g1.inputs) {
		let s = state[i];
		s = ~s;
		s = Math.abs(s);
		sum &= s;
	}
	return sum;
}
