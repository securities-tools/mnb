import * as utils from '../../../utils.js';

export{
	Genome as default
};

const mutation_rate = 0.003;
const mutation_scale = 0.001;
const lenGenome = 1024**2 / Uint32Array.BYTES_PER_ELEMENT;
//const lenGenome = 1024 / Uint32Array.BYTES_PER_ELEMENT;


class Genome extends Uint32Array {
	constructor(genes){
		genes = genes || (Math.random()*Genome.MAX_GENOMESIZE);

		let meta = {
			parents: new Set(),
			generation: 0
		};
		if( typeof genes === 'object'){
			if('parents' in genes.meta) meta.parents = new Set(genes.meta.parents);
			if(genes.meta.karma) meta.karma = genes.meta.karma;
			if('generation' in genes.meta) meta.generation = genes.meta.generation;
			if('genome' in genes) genes = genes.genome.slice(0);
		}
		if(typeof genes === 'string'){
			let bytes = utils.B64ToUInt8(genes);
			genes = bytes.buffer;
		}
		super(genes);

		this.meta = meta;
		if(typeof genes === 'number'){
			for (let i=this.length-1; i >= 0; i--) {
				let r = Math.floor(Math.random() * Genome.MAX_GENE);
				this[i] = r;
			}
		}
		this.pos = 0;
	}

	get id(){
		let i = hash(this);
		return i;
	}

	get GeneScale(){
		return Genome.MAX_GENE;
	}
	get GeneScaleBits(){
		return 8*Genome.BYTES_PER_ELEMENT;
	}

	get peek(){
		let val = this.pos;
		val = this[val];
		val = Math.floor(val);
		return val;
	}
	get peekRatio(){
		let val = this.peek;
		val = val / this.GeneScale;
		return val;
	}

	get next(){
		let val = this.peek;
		this.pos++;
		return val;
	}
	get nextRatio(){
		let val = this.next;
		val /= this.GeneScale;
		return val;
	}

	get pos(){
		return this._pos;
	}
	set pos(val){
		this._pos = (Math.floor(val) + this.length) % this.length;
	}

	shift(){
		return this.next;
	}

	pop(){
		return this.next;
	}

	push(){
		console.warn("Illegal Operation: PUSH to GENOME");
	}

	unshift(){
		console.warn("Illegal Operation: UNSHIFT to GENOME");
	}

	slice(){
		let g= new Genome(this);
		//g.meta = Object.assign(this.meta);
		return g;
	}

	clone(){
		return this.slice();
	}

	mutate(rate=mutation_rate) {
		let genome = this;
		for (let m = Math.floor(this.length*rate); m>=0; m--) {

			let loc = Math.floor(Math.random() * this.length);
			// It is important that it not be 50%. Over time, we want
			// change to occur however, if the amount of change each time
			// is 50/50, the net change over time will be zero. Therefore
			// we need to favour one direction or the other.
			//  - It doesn't really matter which.
			//  - This is not an optimal solution.
			let val = (Math.random()-0.55)*2;
			val	*= maxGene;
			val *= mutation_scale;
			val  = Math.floor(val);
			val += genome[loc];
			val += maxGene;
			val %= maxGene;
			genome[loc] = val;
		}
		return genome;
	}

	mate(that) {
		let rtn = this.clone();
		that = that || rtn;

		rtn.meta.parents.add(that.id);

		rtn.meta.karma *= rtn.meta.parents.size - 1;
		rtn.meta.karma += that.meta.karma;
		rtn.meta.karma /= rtn.meta.parents.size;
		/*
		rtn.meta.generation *= 1.0;
		rtn.meta.generation *= rtn.meta.parents.size - 1;
		rtn.meta.generation += that.meta.generation;
		rtn.meta.generation /= rtn.meta.parents.size;
		rtn.meta.generation = Math.ceil(rtn.meta.generation);
		*/
		rtn.meta.generation = Math.max(rtn.meta.generation, that.meta.generation);
		rtn.meta.generation = Math.floor(rtn.meta.generation);
		rtn.meta.generation += 1;

		for (let pos=this.length-1; pos >= 0;) {
			let section_length = Math.randomIntFromInterval( lenGenome**0.5, lenGenome/2 );
			let copyside = Math.floor(Math.random() * 2);
			if(copyside % 2 === 0){
				pos -= section_length;
			}
			else for(let i=section_length; i>=0; i--,pos--){
				rtn[pos] = that[pos];
			}
		}

		return rtn;
	}

	toString(){
		/*
		let a = JSON.stringify(new Array(20).fill(0).map(d=>{return Math.floor(Math.random()*256);}));
		let b = JSON.parse(a);
		b = new Uint8Array(b);
		b = utils.UInt8ToB64(b);
		b = utils.B64ToUInt8(b);
		b = Array.from(b);
		b = JSON.stringify(b);
		console.log('Valid : ' + (a === b));
		*/

		let bytes = new Uint8Array( this.buffer );
		bytes = utils.UInt8ToB64(bytes);
		return bytes;
	}
}


Genome.MAX_GENE = 2**(8*Genome.BYTES_PER_ELEMENT);
const maxGene = Genome.MAX_GENE;
Genome.MAX_GENOMESIZE = lenGenome;

function hash(a){
	let h = utils.HashArray(a);
	return h;
}
