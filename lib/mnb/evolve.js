import Bug from '../../animat.js';
import Genome from './brain/genome.js';

import * as db from './database.js';
import * as utils from '../../utils.js';


export{
	seedGuf,
	pullFromGuf,
	definePurpose
};

function RandTopWieghted(){
	let x = Math.random();
	x *= 0.5;
	x *= Math.PI;
	let y = Math.sin(x);
	y += 1;
	y /= 2;
	return y;
}

async function pickRandomGenome(range=db.maxItems){
	let i = RandTopWieghted();
	i  = 1 - i;
	i *= range;
	i  = Math.floor(i);

	let gen = await db.getBug(i);
	gen = gen.rows.pop() || null;
	if(gen){
		gen = gen.doc;
		gen.ova--;
		gen.ova = Math.ceil(gen.ova);
		gen.ova = Math.max(gen.ova,0);
		await utils.Pauser(()=>{return db.db.guf.put(gen)});
		let genome = new Genome(gen._attachments.genome.data);
		// we don't want to track grandparents
		//genome.meta.parents = new Set(gen.genome.parents);
		genome.meta.karma = gen.genome.karma;
		genome.meta.generation = gen.genome.generation;
		gen = genome;
	}

	return gen;
}


async function seedGuf(bug) {
	// save it to the guff
	await db.saveBug(bug);
}

async function pullFromGuf(){
	// pick the number of parents to use
	let parents = RandTopWieghted();
	parents = Math.round(2 * parents);
	parents = new Array(parents).fill(pickRandomGenome);
	// for each parent, breed them together
	let bug = null;
	for(let parent of parents){
		let gen = await parent();
		if(!gen) continue;
		//gen = gen || new Genome();
		bug = bug || new Bug(gen);
		bug.genome = bug.genome.mate(gen);
	}
	// if no parents were selected,
	// spontaneously generate one
	bug = bug || new Bug();
	bug.genome.mutate();

	let i = 0 //await FixAct(bug);
	if(i > 0){
		console.log(`\t${i} mutations applied to [${bug.id}]`);
		if(bug.genome.meta.karma){
			i *= Math.sign(bug.genome.meta.karma);
			bug.genome.meta.karma -= i;
			bug.genome.meta.karma = Math.max(bug.genome.meta.karma,0);
		}
	}

	bug._id = null;
	// send it back
	return bug;
}

let _dbsize = null;
setInterval(()=>{
	_dbsize = null
},600000);
async function getDbSize(){
	if(_dbsize !== null) return _dbsize;

	_dbsize = new Promise(async (resolve)=>{
		async function tryit(){
			try{
				let size = await db.db.facts.info();
				resolve(size.doc_count);
			}
			catch(e){
				if(e.code !== 'ECONNRESET'){
					let size = {doc_count:0};
					resolve(size.doc_count);
				}
				setTimeout(tryit,16);
			}
		}
		tryit();
	});

	return _dbsize;
}

async function definePurpose(){
	let record = await getDbSize();
	record *= Math.random();
	record = Math.floor(record);
	let fact = await db.db.facts.allDocs({
		skip: record,
		limit: 1,
		include_docs: true
	});

	let purpose = fact.rows.pop();
	purpose = purpose.id.split('/').map(d=>{return parseInt(d,36);});
	purpose = {
		'comp': purpose[1],
		'date': purpose[4]
	};

	return purpose;
}

/**
 * if the bug is never going to take any
 * action, there is no reason to keep it.
 * Do a mutation and hope that fixes it, or
 * take a random chance that we skip out
 * (to avoid an infinite loop)
 */
async function FixAct(bug){
	let i = 0;
	let willact = false;
	console.log(`\tBrain Activity Mutation`);
	for(; !willact && i < 1000; i++){
		bug.genome.mutate();
		bug.init();
		willact = WillAct(bug);
		//if(i%9 === 0){
		//	process.stdout.write(`\tBrain Activity Mutation: ${i}\r`);
		//}
	}
	if(!willact){
		ForceAct(bug);
	}
	return i;
}

/**
 * after all of that, it still isn't acting. We are going to
 * take extreme measures: cut it open, and manually rewire it
 * to act.
 */
function ForceAct(bug){
	let min = bug.actuators.byteOffset;
	let rng = bug.actuators.length-1;
	for(let gate of bug.brain.gates){
		for (let o in gate.outputs) {
			// take a small chance there will be a change
			let shouldChange = Math.random();
			shouldChange *= bug.brain.gates.length;
			shouldChange = !Math.floor(shouldChange);
			if(shouldChange){
				// forcefully set the output to an action
				let output  = Math.random();
				output *= rng;
				output += min;
				output = Math.floor(output);
				gate.outputs[o] = output;
			}
		}
	}
	// if this thing had karma, it doesn't anymore
	bug.genome.meta.karma = 0;
}

function WillAct(bug){
	let min = bug.actuators.byteOffset;
	let max = min + bug.actuators.length-1;
	for(let gate of bug.brain.gates){
		for (let output of gate.outputs) {
			if(output >= min && output <= max){
				return true;
			}
		}
	}
	return false;
}
