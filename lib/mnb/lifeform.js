import {EventEmitter} from 'events';

import Genome from './brain/genome.js';
import {Brain,BrainIO} from './brain/brain.js';

import GateTypes from './brain/gates.js';
import GateStats from './brain/gatesStats.js';

import * as os from 'os';


class LifeForm extends EventEmitter {
	constructor(genome=null) {
		super();
		this.gatetypes = GateTypes.concat(GateStats);
		this.genome = new Genome(genome,this.gatetypes);
		this.experience = [];
		this.parents = [];
		this.init();
	}

	get id(){
		if(this._id) return this._id;

		let mac = os.networkInterfaces();
		mac = Object.values(mac).pop().shift().mac;

		const maxhash = parseInt('3vvvvvv',32);
		function hash(a){
			a = a
				.reduce((a,d)=>{
					return (a<<3|d) % maxhash;
				},0)
				.toString(32)
				.padStart(7,'0')
				;
			return a;
		}

		this._id = [
			hash(mac.split(':').map(d=>{return parseInt(16)})),
			hash(this.Type.split('').map(d=>{return d.charCodeAt(0)})),
			this.genome.id,
		].join('.');

		return this._id;
	}

	toString(){
		return JSON.stringify(this.toObject);
	}

	toObject(){
		return {
			type: this.Type,
			id: this.id,
			genome: this.genome.toString(),
			experience:this.experience,
		};
	}

	get Type(){
		return this.constructor.name;
	}


	init(){
		// Rebuild the animal's brain
		this.genome.pos = 0;
		this.brain = new Brain(this.genome,this.gatetypes);
		this.age = 0;

		this.actuators = new BrainIO(this.brain,0,1);
		this.sensors = new BrainIO(this.brain,this.actuators.length,1);
		this.actuators.reset(0);
	}

	get MIN_REWARD(){
		return -LifeForm.MAX_REWARD;
	}
	get MAX_REWARD(){
		return LifeForm.MAX_REWARD;
	}

	get MAX_AGE(){
		return LifeForm.MAX_AGE;
	}

	get reward(){
		let value = this.experience.positive - this.experience.negative;
		return value;
	}
	set reward(amt){
		amt = Math.floor(amt);
		if(amt === this.reward) return;
		this.emitChange('reward',this.reward,amt);
		amt -= this.reward;
		if(amt < 0){
			this.experience.negative -= amt;
			this.experience.negative = Math.min(this.MAX_REWARD, this.experience.negative);
			this.experience.negative = Math.floor(this.experience.negative);
		}
		else{
			this.experience.positive += amt;
			this.experience.positive = Math.min(this.MAX_REWARD, this.experience.positive);
			this.experience.positive = Math.floor(this.experience.positive);
		}

	}

	get isDone(){
		if(this.age >= this.MAX_AGE){
			setTimeout(()=>{this.emit('complete',this);});
			return true;
		}
		return false;
	}

	clone() {
		let genome = this.genome.clone();
		let bug = new LifeForm(genome);
		return bug;
	}

	measure(){
		if(this.isDone){
			return;
		}
		this.emit('measure');
	}
	think(){
		if(this.isDone){
			return;
		}
		this.brain.think();
		this.emit('think');
	}
	act(){
		if(this.isDone){
			return;
		}
		this.experience.push(Object.assign(this.actuators));
		this.age++;
		this.emit('act');
	}

}

LifeForm.MAX_REWARD = 0xffffffff;
LifeForm.MAX_AGE = 0xfffffffe;
//LifeForm.MAX_AGE = 0xfffe;

export {
	LifeForm as default,
	LifeForm,
	BrainIO
};
