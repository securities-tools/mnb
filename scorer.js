import {EventEmitter} from 'events';
import _ from 'lodash';

import {Pauser} from './utils.js';
import { isPaused } from './lib/mnb/database.js'

export{
	Scorer as default
}

const TAN_INF = Math.atan(2**31);


class Scorer extends EventEmitter{
	constructor(db){
		super();
		this.db = db;
		this._ = {};
		this.scheduleQuery = _.debounce(()=>{this.query();},250,{leading:true,trailing:true});
		this.lastid = null;
		this.init();
	}

	init(){
		this.guf = this.db.guf;
		this.db.guf.changes({live:true,since:'now'}).on('change', ()=>{
			this.scheduleQuery();
		});
		this.db.facts.changes({live:true,since:'now'}).on('change', ()=>{
			this.scheduleQuery();
		});
		setTimeout(()=>{
			this.scheduleQuery();
		},100);
		clearInterval(this.ticker);
		this.ticker = setInterval(async ()=>{
			return this.scheduleQuery();
		},60000)

	}

	get MAX(){
		return Scorer.MAX;
	}

	get MIN(){
		return Scorer.MIN;
	}

	async query(){
		if (isPaused()) {
			return;
		}
		let results = {rows:[]};
		try{
			if(this.db.guf !== this.guf){
				this.init();
			}
			results = await Pauser(()=>{ return this.db.guf.query('scorables/scorables',{
				limit: 5,
				reduce:false,
				include_docs: true
			})});
		}
		catch(e){
			if(e.code === 'EAI_AGAIN'){
				return;
			}
			console.error(e);
			results = {rows:[]};
			return;
		}
		let buffer = [];
		for(let animat of results.rows){
			if (animat.doc.score){
				continue;
			}
			if(animat.id == this.lastid){
				continue;
			}
			this.lastid = animat.id;
			let score = await this.score(animat.doc);
			if(Number.isNaN(score)){
				debugger;
			}
			if(animat.doc.genome.generation === 0){
				animat.doc.genome.karma = score;
			}
			let karma = animat.doc.genome.karma;
			score = (score + karma) / 2.0;
			buffer.push(
				this
					.save(animat,score)
					.then(()=>{
						this.emit('score',animat.doc,score)
					})
			);
		}
		await Promise.all(buffer);
	}

	async save(animat,score,force=false){
		let upsert = this.db.guf.upsert(animat.doc._id,(orig)=>{
			if('score' in orig) return false;

			orig.score = score;
			//if(score <= 0){
			//	orig.ova = 0;
			//	orig._attachments = {};
			//	return orig;
			//}

			let ova = score;
			ova = Math.max(ova,0);
			ova = Math.atan(ova/this.MAX);
			ova /= TAN_INF;
			ova *= 100;
			ova = Math.ceil(ova) + 2;
			orig.ova = ova;

			return orig;
		});
		return upsert;
	}

	/**
	 *
	 * @param animat
	 */
	async score(animat){
		let value = 0;
		let req = animat.bug.req;
		let resp = animat.bug.resp;

		// it did more than nothing
		let lastbug = this._.lastbug || animat.bug;
		for(let r in resp){
			let val = resp[r];
			// has the value changed?
			// and has it varied from bug to bug
			if(val && val !== lastbug.resp[r]){
				value += 1;
			}
		}
		this._.lastbug = animat.bug;

		// reward ending the simulation prior to old age
		if(resp.done){
			//value += 10 * Math.max(0, Bug.MAX_AGE - (animat.bug.age||0) );
			value += Math.floor(0.0005 * (animat.bug.age-1));
		}

		// predicted a company? (or null, \uffff)
		if (resp.predict > Scorer.constants.NullCik){
			value += 100;
		}
		else{
			return value;
		}
		let cik = resp.predict.toString(16).padStart(8,'0') + '-';
		let query = await Pauser(()=>{return this.db.facts.allDocs({
			startkey: cik,
			endkey: 'g',
			limit:1,
			reduce:false
		})});
		query = query.rows.pop();
		if(query && query.id.startsWith(cik)){
			value += 100;
		}
		else{
			return value;
		}

		// not a particularly wide spread
		if(resp.predStart > resp.predFin){
			// doc points for having the range reversed
			let s = resp.predStart;
			resp.predStart = resp.predFin;
			resp.predFin = s;
			value -= 10;
		}
		// non-sense start date
		if(resp.predStart < 1){
			return value;
		}
		// grant points for the prediction being within a 60 day spread
		let spread = resp.predFin - resp.predStart;
		spread = Math.max(spread, 0);
		spread = Math.min(spread, 60);
		spread = (60 - spread) / 60;
		value += 1000 * spread;

		// not too far in the future
		spread = resp.predStart;
		spread = Math.max(spread,0);
		spread = Math.min(spread,10);
		spread = (10-spread) / 10;
		value += 1000 * spread;

		// acurate prediction?
		query = {
			startkey: [resp.predict, resp.predStart],
			endkey: [resp.predict, resp.predFin],
			reduce:true
		};
		query = await this.db.facts.query('prices/pricerange',query);
		query = query.rows.pop();
		let accuracy = 0;
		if(resp.predChg < query.min){
			accuracy = resp.predChg - query.min;
			accuracy /= query.min;
		}
		else if(resp.predChg > query.max){
			accuracy = resp.predChg - query.max;
			accuracy /= query.max;
		}
		else{
			accuracy = 1;
		}
		value += this.MAX * accuracy;

		return value;
	}

	static WillAct(bug){
		let min = bug.actuators.byteOffset;
		let max = min + bug.actuators.length-1;
		for(let gate of bug.brain.gates){
			for (let output of gate.outputs) {
				if(output >= min && output <= max){
					return true;
				}
			}
		}
		return false;
	}

}

Scorer.MAX = 0xfffffffe/2;
Scorer.MIN = -Scorer.MAX;
Scorer.constants = {
	NullCik: 0
};
