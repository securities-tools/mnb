import {LifeForm,BrainIO} from './lib/mnb/lifeform.js';

export default class SecAnimat extends LifeForm{

	constructor(genome=null,sensorConstants={}) {
		super(genome);

		this.sensorConstants = sensorConstants;

		this.init();
	}

	init() {
		super.init();

		// initialize it's sensors
		let brain = this.brain;
		this.actuators = new (class extends BrainIO{
			constructor() { super(brain,0,20); }
			get done()     { return this.asBool(0); }
			get predict()  { return this.asInt(1); }
			get predChg()  { return this.asInt(2); }
			get predStart(){ return this.asRange(3,3650); }
			get predFin()  { return this.asRange(4,3650); }
			get query()    { return this.qryLim > 0; }
			get qrySkp()   { return this.asRange(5,10000); }
			get qryLim()   { return this.asRange(6,10000); }
			get qryGrp()   { return this.asRange(7,20); }
			get qryAsc()   { return this.asBool(8); }
			get qryKey()   {
				let val = this.subarray(9,19);
				return val;
			}
		})();
		let actuators = this.actuators;
		this.sensors = new (class extends BrainIO{
			constructor(){ super(brain,actuators.length,brain.state.length-actuators.length); }
			set comp(v) { this[ 0] = v; }
			set date(v) { this[ 1] = v; }
			get comp()  { return this[ 0]; }
			get date()  { return this[ 1]; }
			set resp(v) {
				// needs to be offset by 2 to leave room for comp and date
				let statesize = brain.state.length - 3;
				for(let i=0; i<v.length; i++){
					let pos = i % statesize;
					brain.state[pos+2] = v[pos];
				}
			}
			get resp(){
				return this.subarray(2,brain.state.length-1);
			}
		})();

		this.actuators.reset();
		this.experience.negative = 0;
		this.experience.positive = 0;
	}

	get MAX_AGE(){
		return 0xffff;
	}

	clone() {
		let genome = this.genome.clone();
		let bug = new Bug(genome);
		return bug;
	}

	toString() {
		return JSON.stringify(this);
	}

	static fromString(json) {
		json = JSON.parse(json);
		let bug = new SecAnimat();
		for (let name in json) {
			if (name in bug) {
				bug[name] = json[name];
			}
		}
	}

	measure(){
		for(let c in this.sensorConstants){
			this.sensors[c] = this.sensorConstants[c];
		}
		super.measure();
	}

	get done(){
		debugger;
		console.error("Deprecation Warning: call to Animat.done use `Animate.isDone` instead");
		return this.isDone();
	}
	get isDone(){
		let done = super.isDone;
		//done = done || this.actuators.done;
		if(done){
			setTimeout(()=>{this.emit('complete',this)});
			return true;
		}
		return false;
	}

	act(){
		super.act();
		//if(this.actuators.done){
		//	this.emit('complete',this);
		//	return;
		//}

		this.prayer = null;
		if(!this.actuators.query) return;

		function KeyStringify(key){
			for(let k in key){
				key[k] = key[k].toString(36);
				key[k] = key[k].padStart(7,'0');
			}
			key = key.join('/');
			return key;
		}

		let sKey = Array.from(this.actuators.qryKey).slice(0,this.actuators.qryGrp);
		let eKey = sKey.slice();
		eKey.push(0xffffffff);
		sKey = KeyStringify(sKey);
		eKey = KeyStringify(eKey);
		this.prayer = {
			startKey: sKey,
			endKey: eKey,
			reduce:true,
			group:true,
			group_level: sKey.length,
			descending:!this.actuators.qryAsc,
			skip: this.actuators.qrySkp,
			limit: this.actuators.qryLim,
			include_docs: true,
		};

	}

}
