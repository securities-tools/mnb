import {EventEmitter} from 'events';
import workerpool from 'workerpool';

import LifeForm from './animat.js';
import * as utils from './utils.js';

export {
	Universe as default
};

class Universe extends EventEmitter{
	constructor(settings) {
		super();

		// https://github.com/josdejong/workerpool
		this.pool = workerpool.pool();
		this.library = settings.db.facts;
		this.capacity = settings.capacity || 100;

		this._ = {};
		this._lifeForms = new Set();
		this._running = null;

		this._.bugFinish = (l)=>{
			this.emit('complete',l);
		};

		this.addListener('complete',(l)=>{
			this.remove(l);
		});

	}

	addEventListener(event,listener){
		this.prependListener(event,listener);
	}

	get lifeForms(){
		return Array.from(this._lifeForms.values());
	}

	add(life){
		if(!Array.isArray(life)){
			return this.add([life]);
		}
		else{
			for(let l of life){
				if (this._lifeForms.size >= this.capacity) break;
				if (l instanceof LifeForm) {
					this._lifeForms.add(l);
					this.emit("add", {detail: l});
					l.addListener('complete',this._.bugFinish);
					setTimeout(()=>{this.tick(l);},100);
				}
			}
		}
		return this;
	}

	remove(life){
		if(!life){
			debugger;
			return;
		}

		if(Array.isArray(life)){
			for(let l of life){
				this.remove(l);
			}
			return;
		}

		life.removeListener('complete',this._.bugFinish);
		let success = this._lifeForms.delete(life);
		if(success){
			this.emit('remove', {detail: life});
		}
	}

	clear(){
		this.remove(this.lifeForms);
	}

	async tick(bug){
		if(!this._lifeForms.has(bug)){
			return;
		}
		bug.measure();
		bug.think();
		bug.act();

		if(bug.age % 357 === 0){
			console.log(`Work..: ${bug.id} -> ${bug.age}`);
		}

		if(bug.prayer){
			let resp = bug.sensors.resp;

			bug.prayer.limit = Math.min(bug.prayer.limit || 100, resp.length);
			let results = await utils.Pauser(()=>{return this.library.allDocs(bug.prayer);});
			results = results.rows.map(d=>d.doc.val).slice(0,resp.length);

			bug.sensors.resp = results;
		}

		setTimeout(()=>{this.tick(bug);});
	}

}
