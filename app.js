import './utils.js';
import './BuildCouch.js';

import Universe from './universe.js';
import Scorer from './scorer.js';

import * as evolve from './lib/mnb/evolve.js';
import * as gates from './lib/mnb/brain/gates.js';
import * as db from './lib/mnb/database.js';


function loadParams(params = {}){
	let rtn = {
		progress: [1,1000],
		iterations: 20000,
		animats: 1,
	};
	rtn = Object.assign(rtn,params);
	params = process.argv
		.map(d=>{
			return d.split('=');
		})
		.filter(d=>{
			return d.length >= 2;
		})
		.reduce((a,d)=>{
			a[d[0]] = d[1];
			return a;
		},{})
		;
	rtn = Object.assign(rtn,params);
	return rtn;
}


async function restore(curState={}){
	let state = await db.readState();
	state = Object.assign(curState,state);
	return state;
}


async function main(){

	let state = await restore();
	state = loadParams(state);
	state.gatetypes = gates.GateTypes.bitGateTypes;
	state.db = db.db;

	let terranium = new Universe(state);
	let scorer = new Scorer(state.db);

	async function addBug(){
		let bug = await evolve.pullFromGuf();
		bug.sensorConstants = await evolve.definePurpose();
		terranium.add(bug);
		console.log(`Born..: ${bug.id} -> ${bug.genome.meta.generation}`);
	}

	terranium.addListener('complete',async (bug)=>{
		console.log(`Died..: ${bug.id} -> ${bug.age}`);
		evolve.seedGuf(bug);
		addBug();
	});
	scorer.addListener('score',(bug,score)=>{
		console.log(`Score.: ${bug.bug.id} -> ${score}`);
	});

	for(let i=state.animats; i>0; i--){
		addBug();
	}
}


try{
	main();
}
catch(e){
	debugger;
	console.log(e);
}
