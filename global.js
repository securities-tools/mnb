import fs from 'fs';
import PouchDB from 'pouchdb';
import pupsert from 'pouchdb-upsert';
import path from "path";

export {
	settings,
	dbs,
}

PouchDB.plugin(pupsert);



let settings = process.argv.slice();
settings.shift();
settings.shift();
settings = settings.join(' ').trim();
if(!settings){
	settings = process.cwd();
	settings = `${settings}/secrets.json`;
}
settings = path.normalize(settings);
settings = path.resolve(settings);
console.log("Loading: " + settings);
settings = JSON.parse(fs.readFileSync(settings, 'utf8'));



const dbs = {
	facts: null,
	guf: null,
};



let opts = {};
if(settings.storage.ibmgrant.username && settings.storage.ibmgrant.password){
	opts.auth = {
		username: settings.storage.ibmgrant.username,
		password: settings.storage.ibmgrant.password,
	};
}
dbs.facts = new PouchDB(settings.storage.ibmgrant.url,opts);



let connect = ()=>{
	opts = {};
	if(settings.storage.ibmmnb.username && settings.storage.ibmmnb.password){
		opts.auth = {
			username: settings.storage.ibmmnb.username,
			password: settings.storage.ibmmnb.password,
		};
	}
	dbs.guf = new PouchDB(settings.storage.ibmmnb.url,opts);
	dbs.guf.connect = connect;
}
connect();

for(let db in dbs){
	dbs[db].allDocs({limit:1})
		.then(r=>{
			console.log(r);
			if(r){
				console.log(`Test Connection successful (${db})`);
			}
		}).catch(e=>{
			console.error(`failed to instantiate database connection (${db})`);
			console.error(e);
			process.exit(2);
		})
		;
}
